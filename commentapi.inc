<?php

/**
 * @file
 * Comment API functions to enhance core comment module.
 */

/**
 * Load the entire comment by cid.
 *
 * Taken from _comment_load() with addition of a static cache.
 *
 * @param $cid
 *   Int. The identifying comment id.
 * @return
 *   Object. The comment object.
 */
function commentapi_load($cid, $reset = FALSE) {
  static $comments = array();

  if ($reset) {
    $comments = array();
  }

  if (isset($comments[$cid])) {
    return is_object($comments[$cid]) ? drupal_clone($comments[$cid]) : $comments[$cid];
  }

  return db_fetch_object(db_query('SELECT * FROM {comments} WHERE cid = %d', $cid));
}

/**
 * Helper for moving comments from one node to another.
 *
 * @param $cids
 *  Array. Array of comment IDs to move.
 */
function commentapi_transfer($cids = array()) {
  if (empty($cids) || !is_array($cids)) {
    return FALSE;
  }

  $sql = "UPDATE {comments}
          SET nid = %d
          WHERE cid IN (" . db_placeholders($cids, 'int') . ")";
  db_query($sql, $to_nid);
}

/**
 * Get children of passed cids.
 *
 * @param $cids
 *  Mixed. Either a single comment ID or an array of comment IDs.
 * @param $return_type
 *  String. Type of data to return. Options:
 *    -- simple_array
 *    -- nested_array
 *
 * @return
 *  Array. Multi-dimensional array of comment children cids.
 */
function commentapi_get_children($cids, $return_type = 'simple_array') {
  if (empty($cids) || !is_array($cids)) {
    return FALSE;
  }

  $children = array();

  foreach ($cids as $key => $cid) {
    $children[$cid] = array();
    $sql = "SELECT thread
            FROM {comments}
            WHERE c.cid = %d";
    $thread = trim(db_fetch_result(db_query($sql, $cid)), '/');
    $sql = "SELECT cid
            FROM {comments}
            WHERE thread LIKE '%s.%%'
            ORDER BY thread DESC";
    $result = db_query($sql, $thread);
    while ($row = db_fetch_object($result)) {
      $children[$cid][] = $row->cid;
    }
  }

  switch ($return_type) {
    case 'simple_array':
      return $children;
      break;

    case 'nested_array':
      return commentapi_nest_children($children);
      break;
  }
}

function commentapi_nest_children($children, $depth = 0) {
  $nested = array();
  foreach ($children as $parent) {
    $new_depth = substr_count($parent, '.');
    if (is_array($parent) || ($new_depth > $depth)) {
      $nested = commentapi_nest_children($parent, $new_depth);
    }
    else {
      $nested[] = $parent;
    }
  }
  return $nested;
}

/**
 * Get all comments after passed cids.
 *
 * @param $cids
 *  Array. Array of comment IDs to get comments after.
 *
 * @return
 *  Array. Flat array of comment after cids.
 */
function commentapi_get_commments_after($cids) {
  $after = array();

  return $after;
}

/**
 * Delete comment(s) with options to handle replies.
 */
function commentapi_delete($cids, $children_action = 'nothing') {
  if (is_numeric($cids)) {
    $cids = array($cids);
  }
  if (empty($cids) || !is_array($cids)) {
    return FALSE;
  }

  foreach ($cids as $cid) {
    $comment = commentapi_load($cid);
    switch ($children_action) {
      case 'delete':
        _comment_delete_thread($comment);
        _comment_update_node_statistics($comment->nid);
        break;

      // Move children up the parent of the comment being deleted.
      case 'move_up':
        comment_delete_move_replies($comment);
        _comment_delete_thread($comment);
        _comment_update_node_statistics($comment->nid);
        break;

      // Delete only this comment, or replace the subject/body of replies with a notice.
      case 'nothing':
      default:
        if (comment_num_replies($comment->cid)) {
          $sql = "UPDATE {comments}
                  SET subject = '', comment = '%s'
                  WHERE cid = %d";
          // This should be customizable by being passed to the function.
          db_query($sql, t('This comment has been deleted.'), $comment->cid);
        }
        else {
          _comment_delete_thread($comment);
          _comment_update_node_statistics($comment->nid);
        }
    }
  }

  cache_clear_all();
}

/**
 * Moves a comment within the same node.
 */
function commentapi_move($cid, $move_type) {

}

/**
 * Convert a comment to a node.
 */
function commentapi_convert_comment($cid, $node_type) {

}

/**
 * Convert a node to a comment.
 */
function commentapi_convert_node($cid, $node_type) {

}
